echo Please follow https://freckled.dev/how-to/2019/07/29/create-gcc-crosscompiler/ up to 'Absolute Softlinks' if you have not already.

piname=pi@rpirobot.attlocal.net
dirname=cross_rpi
arch=armv6 #can use 64-bit arm on pi 3+ if you have a 64-bit OS.
tune=cortex-a53 #pi3 processor
gcc_version='10.2.0'
binutils_version='2.35'

set -e

mkdir $dirname
cd $dirname

# variables that define the result
install_dir="$(pwd)/install"
sysroot_dir="$(pwd)/sysroot"

# mount raspberry root
mkdir sysroot_direct
sshfs -o rw $piname:/ ./sysroot_direct
#rw allows us to also work on the project from here

#We aren't going to directly utilize the sysroot, because the /etc/ld.so.conf file on the rpi will
#not work for us. I don't know why, this information comes from: https://sysprogs.com/w/fixing-rpath-link-issues-with-cross-compilers/

#To work around, we've mounted our sysroot elsewhere and will create a bunch of symlinks.
#We will populate /etc ourselves, since we don't need much from there.

mkdir sysroot
cd sysroot
ln -s ../sysroot_direct/usr usr
ln -s ../sysroot_direct/opt opt
ln -s ../sysroot_direct/lib lib
ln -s ../sysroot_direct/home home

mkdir etc
echo "/opt/vc/lib
 
/lib/arm-linux-gnueabihf
/usr/lib/arm-linux-gnueabihf
 
/usr/local/lib" > etc/ld.so.conf

cd ..

# get sources
curl -Lo binutils.tar.bz2 \
  "https://ftpmirror.gnu.org/binutils/binutils-$binutils_version.tar.bz2"
curl -Lo gcc.tar.xz \
  "https://ftp.wayne.edu/gnu/gcc/gcc-$gcc_version/gcc-$gcc_version.tar.xz"

# build binutils
mkdir binutils_source
cd binutils_source
tar --strip-components 1 -xf ../binutils.tar.bz2
mkdir ../binutils_build
cd ../binutils_build
../binutils_source/configure \
  --prefix="$install_dir" \
  --target=arm-linux-gnueabihf \
  --with-arch=$arch \
  --with-tune=$tune \
  --with-fpu=vfp \
  --with-float=hard \
  --disable-multilib \
  --with-sysroot="$sysroot_dir" \
  --enable-gold=yes \
  --enable-lto
make -j$(nproc)
make install
cd ..

# build gcc
mkdir gcc_source
cd gcc_source
tar --strip-components 1 -xf ../gcc.tar.xz
mkdir ../gcc_build
cd ../gcc_build
../gcc_source/configure \
  --prefix="$install_dir" \
  --target=arm-linux-gnueabihf \
  --with-arch=$arch \
  --with-tune=$tune \
  --with-fpu=vfp \
  --with-float=hard \
  --disable-multilib \
  --with-sysroot="$sysroot_dir" \
  --enable-languages=c,c++
make -j$(nproc)
make install
cd ..

echo "set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR armv7l)

set (CMAKE_SYSROOT $PWD/sysroot/)
set (CMAKE_FIND_ROOT_PATH \${CMAKE_SYSROOT})

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE BOTH)" > cross.cmake

echo "#!/bin/bash

export CMAKE_SYSROOT=$PWD/sysroot/
export CMAKE_FIND_ROOT_PATH=\$CMAKE_SYSROOT
export CMAKE_PREFIX_PATH=\$CMAKE_SYSROOT/usr/local/lib/cmake/
export PATH=$PWD/install/bin:\$PATH

alias gcc=arm-linux-gnueabihf-gcc
alias cc=gcc
alias g++=arm-linux-gnueabihf-g++
alias cxx=g++

export CXX=arm-linux-gnueabihf-g++ 
export CC=arm-linux-gnueabihf-gcc

export tchain=-DCMAKE_TOOLCHAIN_FILE=\$PWD/cross.cmake" > environment.sh


echo "Done. Use \"source environment.sh\" to set up your shell environment to cross-compile.
For CMake users, make sure to use the \$tchain variable at the end of your command to easily
point cmake to the toolchain file, like \"cmake . \$tchain\""
echo "Note: Your RPi system has been mounted read/write at $dirname/sysroot_direct. If you wish to delete $dirname then please umount $dirname/sysroot_direct first to avoid nuking your raspberry pi."
